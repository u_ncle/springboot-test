package com.css.springboottest;


import com.css.springboottest.mapper.*;
import com.css.springboottest.pojo.*;
import com.css.springboottest.util.GltfRead;
import com.css.springboottest.util.IOUtils;
import com.css.springboottest.util.RedisUtils;
import com.css.springboottest.util.ZpUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootTest
class
SpringbootTestApplicationTests {
//    public final String[] gltfDir={"A5-1","A5-9.10","A5-11.12.13.14","A5-S1.S2.S3.4.5.6.7"};
//  @Resource
//    DataSource dataSource;
//    @Test
//    void contextLoads() {
////        System.out.println(this.dataSource);
//      String version= SpringBootVersion.getVersion();
//      System.out.println(version);
//    }
//    @Autowired
//   DirStoreMapper dirStoreMapper;
//    String DirId="";
////    插入dirStore  A5-10
//    @Test
//    void insertDir(){
//
//        for (int i = 0; i < gltfDir.length; i++) {
//            DirStore dirStore=new DirStore();
//            dirStore.setDirName(gltfDir[i]);
//            int insert=dirStoreMapper.insert(dirStore);
//        }
//    }
//    @Autowired
//    HouseStoreMapper houseStoreMapper;
//    //填充A5-10 的house
//    @Test
//    void insertHouse(){
//        DirId=dirStoreMapper.selectDirid();
//        HouseStore houseStore=new HouseStore();
//        String res=readJsonFile("json/A5-10/A5_10_loufang.gltf");
//        houseStore.setHouseName("A-5-10-h1");
//        houseStore.setDirId(DirId);
//        houseStore.setJson(res);
//        int insert=houseStoreMapper.insert(houseStore);
//    }
//
//    //填充A5-10d1window
//    @Autowired
//    WindowStoreMapper windowStoreMapper;
//    @Test
//    void insertWindow(){
//        DirId=dirStoreMapper.selectDirid();
//        List<String>windowJson=getWindowJson("json/A5_10—chuanghu.gltf");
//        for (int i = 0; i < windowJson.size(); i++) {
//            WindowStore windowStore=new WindowStore();
//            windowStore.setWindowId(i);
//            windowStore.setWindowName("A-5-10-h1-w"+i);
//            windowStore.setDirId(DirId);
//            windowStore.setJson(windowJson.get(i));
//            int insert=windowStoreMapper.insert(windowStore);
//        }
//    }
////    测试redis链接成功
//    @Autowired
//    RedisTemplate redisTemplate;
//    public String ping(){
//        return (String) redisTemplate.execute(new RedisCallback<String>() {
//            @Override
//            public String doInRedis(RedisConnection connection) throws DataAccessException {
//                return connection.ping();
//            }
//        });
//    }
//    @Test
//    void testPing(){
//        System.out.println(ping());
//
//    }
//
//    //将窗户信息写入redis
//    @Autowired
//    RedisUtils redisUtils;
////
////    @Test
////    void insertRedis(){
////
////        List<String>windowJson=getWindowJson(  "D:\\testClick\\jianzhu_A5_1_3.gltf");
////        try {
//////            redisTemplate.multi();
////            for (int i = 0; i < windowJson.size(); i++) {
////                //键值为建筑栋数 +？？？
////                redisUtils.set(String.valueOf(i),windowJson.get(i));
////                System.out.println("写入:"+i);
////            }
//////            redisTemplate.exec();
////        } catch (Exception e) {
//////            redisTemplate.discard();
////            e.printStackTrace();
////        }finally {
////            System.out.println("完成"+redisUtils.get("1"));
////
////        }
////
////
////    }
//    //多文件写入redis
////    @Test
////    void ThreadFile(){
////        Long startTime = System.currentTimeMillis();
////        List<String>fileNames=IOUtils.getFiles("D:\\testClick");
////        for (int i = 0; i < fileNames.size(); i++) {
////            //读
////            String BH=IOUtils.getBuildingBH(fileNames.get(i));
////            List<String>gltfJaon=GltfRead.getWindowJson(fileNames.get(i));
////            //写
////            try {
////                for (int j = 0; j < gltfJaon.size(); j++) {
////                    redisUtils.set(BH+"_w_"+j,gltfJaon.get(j));
////                    System.out.println("写入"+BH+"_w_"+j);
////                }
////            } catch (Exception e) {
////                e.printStackTrace();
////            } finally {
////                System.out.println("完成"+fileNames.get(i));
////            }
////        }
////        System.out.println("执行耗时:"+(System.currentTimeMillis()-startTime)+"");
////    }
//    //多线程多文件写入redis---------------------------------------------------------
//
//@Test
//    void PCFile(){
//    Long startTime = System.currentTimeMillis();
//    SynContainer synContainer=new SynContainer();
//    List<Thread> list = new ArrayList<>();
//    List<String>fileNames=IOUtils.getFiles("D:\\testClick");
//    Thread readThread= new  ReadGltf(synContainer,fileNames);
//    readThread.start();
//    list.add(readThread);
//    Thread writeThread= new  WriteGltf(synContainer,fileNames);
//    writeThread.start();
//    list.add(writeThread);
//    try {
//        // 让每一个子线程都阻塞,等到
//        for(Thread thread : list) {
//            // 每循环一次都将阻塞一次，直到该子线程执行完毕后，再继续循环执行下一个子线程的join
//            thread.join();
//        }
//    } catch (InterruptedException e) {
//        e.printStackTrace();
//    }
//    Long endTime = System.currentTimeMillis();
//    System.out.println("所有子线程执行完毕了。。。");
//
//    System.out.println("执行耗时:"+(System.currentTimeMillis()-startTime)+"");
//
//}
//
//    }
//class ReadGltf extends Thread{
//   SynContainer synContainer;
//    List<String>fileNames;
//    public ReadGltf(SynContainer synContainer, List<String>fileNames){
//        this.synContainer=synContainer;
//        this.fileNames=fileNames;
//    }
//
//    @Override
//    public void run() {
//        for (int i = 0; i <fileNames.size(); i++) {
//            synContainer.readFile(fileNames.get(i));
//
//
//        }
//    }
//}
//class WriteGltf extends Thread{
//  SynContainer synContainer;
//    List<String>FileNames;
//    public WriteGltf(SynContainer synContainer, List<String>FileNames){
//        this.synContainer=synContainer;
//        this.FileNames=FileNames;
//
//    }
//
//    @Override
//    public void run() {
//        for (int i = 0; i <FileNames.size(); i++) {
//            synContainer.writeFile();
//        }
//    }
//}
//
//class SynContainer extends Thread{
//
//    private RedisUtils redisUtils = BeanUtils.getBean(RedisUtils.class);
//    List<String>container;
//    String fileName;
//    boolean flag=true;  //为true时读  为false时写
//    //读取文件
//    public synchronized void  readFile(String fileName){
//        List<String>res=new ArrayList<>();
//        //flag为false 停止读取，开始写入文件
//        if(!flag){
//            try {
//                this.wait();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//        res= GltfRead.getWindowJson(fileName);
//        System.out.println("信号灯读取->"+fileName);
//        this.notifyAll();
//        this.container=res;
//        this.fileName=fileName;
//        this.flag=!this.flag;
//
//
//
//    }
//    //写入redis
//    public synchronized void writeFile(){
//        if(flag){
//            try {
//                this.wait();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        System.out.println("信号灯写入->"+this.fileName);
//        String BH= IOUtils.getBuildingBH(this.fileName);
//                try {
//                    for (int i = 0; i <this.container.size(); i++) {
//                        redisUtils.set(BH+"_w_"+i,this.container.get(i));
//                        redisUtils.sSet(BH,BH+"_w_"+i);
//                        System.out.println("redis写入->"+BH+"_w_"+i);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }finally {
//        this.notifyAll();
//        this.flag=!this.flag;
//                }
//    }
////    --------------------------------------------插入zp mysql表
////    插入社区表
//    @Autowired
//    CommunityMapper communityMapper;
//    @Test
//    void  insertCommunity(){
//        String communityId= UUID.randomUUID().toString().replaceAll("-","");;
//        Community community=new Community(communityId,"钟屏街道木城社区");
//        int insert=communityMapper.insert(community);
//    }
////    插入楼栋表
//    @Autowired
//    BuildMapper buildMapper;
//    @Test
//    void insertBuild(){
//        String commId=communityMapper.getCommunityId();
//        List<String>fileNames=IOUtils.getFiles("D:\\testClick");
//        for (int i = 0; i < fileNames.size(); i++) {
//            //读
//            String BH=IOUtils.getBuildingBH(fileNames.get(i));
//            //写
//            Build build=new Build();
//            build.setZpBuildName(BH);
//            build.setZpCommunityId(commId);
//            int insert=buildMapper.insert(build);
//        }
//
//    }
////    插入房号表
//    @Test
//    void insertRoom(){
//
//
//    }
////    插入窗户表
//    @Autowired
//    WindowMapper windowMapper;
//    @Test
//    void  insertWindow(){
//        List<String>fileNames=IOUtils.getFiles("D:\\testClick");
//        for (int i = 0; i < fileNames.size(); i++) {
//            //读
//            String BH=IOUtils.getBuildingBH(fileNames.get(i));
//            int windowNum=GltfRead.getWindowNum(fileNames.get(i));
//            for (int j = 0; j < windowNum; j++) {
//                Window window=new Window();
//                window.setZpWindowId(BH+"_w_"+j);
//                window.setZpWindowName("_w_"+j);
//                int insert=windowMapper.insert(window);
//            }
//
//        }
//
//    }

    //room表填充
    @Autowired
    RoomMapper roomMapper;
    @Autowired
    HouseholdMapper householdMapper;
    @Test
    void insertRoom(){
//     1.根据household插入room   zp_room_id
//        取去重的房间号
        List<String>rooms=householdMapper.getRooms();
        for (int i = 0; i < rooms.size(); i++) {
            Room room=new Room();
            room.setZpRoomId(rooms.get(i));
            room.setZpBuildName(ZpUtils.getBuildName(rooms.get(i)));
            room.setZpUnitName(ZpUtils.getUnitName(rooms.get(i)));
            room.setZpRoomName(ZpUtils.getRoomName(rooms.get(i)));
            room.setIsbind(0);
            roomMapper.insert(room);
        }
    }

    @Test
    void insertHousehold(){
        Household household=new Household();
        household.setZpHouseholdId("530326199855770441");
        household.setZpRoomId("A5_5_3_102");
        household.setZpHouseholdName("王五");
        householdMapper.insert(household);
    }



}




