package com.css.springboottest.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Attributes {
    @JsonProperty("POSITION")
    @JSONField(name="POSITION")
    private int POSITION;
    @JsonProperty("NORMAL")
    @JSONField(name="NORMAL")
    private int NORMAL;
    @JsonProperty("TEXCOORD_0")
    @JSONField(name="TEXCOORD_0")
    private int TEXCOORD_0;
}
