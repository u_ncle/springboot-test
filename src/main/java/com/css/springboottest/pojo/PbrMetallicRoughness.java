package com.css.springboottest.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PbrMetallicRoughness {
    private BaseColorTexture baseColorTexture;
    private double[]baseColorFactor;
    private int metallicFactor;
    private double roughnessFactor;

}
