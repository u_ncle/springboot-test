package com.css.springboottest.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("zp_window_store")
public class WindowStore implements Serializable {

    private static final long serialVersionUID = 1L;

//    @TableId(value = "window_id", type = IdType.ASSIGN_ID)
    @TableId(value = "window_id",type = IdType.INPUT)
    private int windowId;

    private String windowName;

    private String dirId;

    private String json;


}
