package com.css.springboottest.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Samplers {
    private int magFilter;
    private int minFilter;
    private int wrapS;
    private int wrapT;
}
