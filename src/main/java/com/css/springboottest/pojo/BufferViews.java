package com.css.springboottest.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BufferViews {

    private int buffer;
    private int byteOffset;
    private int byteLength;
    private int target;
    private int byteStride;


}
