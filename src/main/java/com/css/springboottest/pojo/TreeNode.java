package com.css.springboottest.pojo;

import com.alibaba.fastjson.JSONObject;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//树结构拓扑
public class TreeNode implements Serializable {
//    private TreeNode parent;
    private  String parentName;
    private int isBind=0;
    private String name;
    private List<TreeNode>childList;



    public TreeNode(String parentName, String name, int isBind) {
        this.parentName = parentName;
        this.name = name;
        this.isBind=isBind;


    }


//
//    public int getLevel(){
//
//        return parent == null?0:parent.getLevel() + 1;
//    }
    //添加子节点
    public void addChildNode(TreeNode node){
        if (childList == null){
            childList = new ArrayList<>();
        }
        childList.add(node);
    }
    //清空子节点
    public void clearChildren(){
        if (childList != null){
            childList.clear();
        }
    }

}
