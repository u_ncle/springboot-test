package com.css.springboottest.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GltfModel {
private List<Object>bufferViews;
private Asset asset;
private List<Scene>scenes;
private List<Nodes>nodes;
private List<Meshes>meshes;
private List<Accessors> accessors;
private List<Buffers>buffers;
private List<Images>images;
private List<Samplers>samplers;
private List<Textures>textures;
private List<Materials>materials;
}
