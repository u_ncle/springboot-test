package com.css.springboottest.pojo;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//父节点
public class TreeParent {
    //自身id
    private String id;
    //父节点id
    private String parentId;
   //名称
    private String name;
    //是否绑定
    private int isBind;



    public TreeParent(String id, String parentId, String name) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;

    }
}
