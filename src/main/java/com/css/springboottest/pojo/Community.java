package com.css.springboottest.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("zp_community")
@AllArgsConstructor
@NoArgsConstructor
public class Community implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "zp_community_id", type = IdType.INPUT)
    private String zpCommunityId;

    private String zpCommunityName;

    public Community(String zpCommunityName) {
        this.zpCommunityName = zpCommunityName;
    }
}
