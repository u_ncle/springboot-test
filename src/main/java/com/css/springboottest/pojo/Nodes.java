package com.css.springboottest.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Nodes {
    private String name;
    private List<Double>translation;
    private List<Double>rotation;
    private List<Double>scale;
    private int mesh;

}
