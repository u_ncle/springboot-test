package com.css.springboottest.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("zp_dir_store")
public class DirStore implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "dir_id", type = IdType.ASSIGN_ID)
    private String dirId;

    private String dirName;


}
