package com.css.springboottest.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("zp_window")
public class Window implements Serializable {

    private static final long serialVersionUID = 1L;

    private String zpRoomId;

    @TableId(value = "zp_window_id", type = IdType.INPUT)
    private String zpWindowId;

    private String zpWindowName;


}
