package com.css.springboottest.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.github.jeffreyning.mybatisplus.anno.MppMultiId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("zp_household")
@AllArgsConstructor
@NoArgsConstructor
public class Household implements Serializable {

    private static final long serialVersionUID = 1L;
    //栋号+单元号+房号
    @MppMultiId
    @TableField("zp_room_id")
    private String zpRoomId;
    //身份证号
//    @TableId(value = "zp_household_id", type = IdType.INPUT)
    @MppMultiId
    @TableField("zp_household_id")
    private String zpHouseholdId;
     //  姓名
    private String zpHouseholdName;
    //与户主关系
    private String zpHouseholdIdentity;
//    手机号
    private String zpHouseholdPhone;
//治安危险类型
    private String zpHouseholdDangerType;
//成员状态
    private String zpHouseholdState;
//政治面貌
    @TableField("zp_household_Politics")
    private String zpHouseholdPolitics;
//性别
    private Integer zpHouseholdSex;
//年龄
    private Integer zpHouseholdAge;
//    住址
    @TableField("zp_household_adress")
    private String zpHouseholdAdress;
//备注 ["困难家庭", "整户无劳力", "就业帮扶"]
    private String zpHouseholdTips;
//犯罪记录
    private String zpHouseholdCriminal;
//其他说明
    private String zpHouseholdOther;
//管控措施
    @TableField("zp_household_measure")
    private String zpHouseholdMeasure;
//帮扶政策
    @TableField("zp_household_help")
    private String zpHouseholdHelp;

   //某户人员基本信息
    public Household(String zpHouseholdId, String zpHouseholdName, String zpHouseholdIdentity, String zpHouseholdPhone, String zpHouseholdDangerType, String zpHouseholdState, String zpHouseholdPolitics, Integer zpHouseholdSex, Integer zpHouseholdAge) {
        this.zpHouseholdId = zpHouseholdId;
        this.zpHouseholdName = zpHouseholdName;
        this.zpHouseholdIdentity = zpHouseholdIdentity;
        this.zpHouseholdPhone = zpHouseholdPhone;
        this.zpHouseholdDangerType = zpHouseholdDangerType;
        this.zpHouseholdState = zpHouseholdState;
        this.zpHouseholdPolitics = zpHouseholdPolitics;
        this.zpHouseholdSex = zpHouseholdSex;
        this.zpHouseholdAge = zpHouseholdAge;
    }
    public Household(String zpHouseholdId, String zpHouseholdName, String zpHouseholdIdentity, String zpHouseholdAdress) {
        this.zpHouseholdId = zpHouseholdId;
        this.zpHouseholdName = zpHouseholdName;
        this.zpHouseholdIdentity = zpHouseholdIdentity;
        this.zpHouseholdAdress = zpHouseholdAdress;

    }

   //某户人员详细信息
    public Household(String zpHouseholdId, String zpHouseholdName, String zpHouseholdPhone, String zpHouseholdAdress, String zpHouseholdCriminal, String zpHouseholdOther, String zpHouseholdMeasure, String zpHouseholdHelp) {
        this.zpHouseholdId = zpHouseholdId;
        this.zpHouseholdName = zpHouseholdName;
        this.zpHouseholdPhone = zpHouseholdPhone;
        this.zpHouseholdAdress = zpHouseholdAdress;
        this.zpHouseholdCriminal = zpHouseholdCriminal;
        this.zpHouseholdOther = zpHouseholdOther;
        this.zpHouseholdMeasure = zpHouseholdMeasure;
        this.zpHouseholdHelp = zpHouseholdHelp;
    }
}
