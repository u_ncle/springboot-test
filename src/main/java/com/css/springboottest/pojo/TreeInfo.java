package com.css.springboottest.pojo;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//叶子节点实体类
public class TreeInfo {
//    自身id
    private String id;
//    父id
    private String groupId;
//    名称
    private String name;
    //是否绑定
    private int isBind;


//

    public TreeInfo(String id, String groupId, String name) {
        this.id = id;
        this.groupId = groupId;
        this.name = name;

    }
}
