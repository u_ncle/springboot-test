package com.css.springboottest.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Accessors {
    private  int bufferView;
    private String type;
    private int componentType;
    private int count;
    private int byteOffset;
    private List<Object> min;
    private List<Object> max;

}
