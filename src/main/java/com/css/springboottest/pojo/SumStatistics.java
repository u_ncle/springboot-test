package com.css.springboottest.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SumStatistics {
//    总园区
    private int parkNum;
//    总楼栋
    private int buildNum;
//    总单元
    private int unitNum;
//    总户数
    private int roomNum;
//    总人口
    private int population;

    public SumStatistics(int buildNum, int unitNum, int roomNum, int population) {
        this.buildNum = buildNum;
        this.unitNum = unitNum;
        this.roomNum = roomNum;
        this.population = population;
    }
}
