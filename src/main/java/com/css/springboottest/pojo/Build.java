package com.css.springboottest.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("zp_build")
public class Build implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "zp_build_id", type = IdType.ASSIGN_ID)
    private String zpBuildId;

    private String zpBuildName;

    private String zpCommunityId;
    @TableField("zp_build_info")
    private String zpBuildInfo;

    public Build(String zpBuildName, String zpCommunityId) {
        this.zpBuildName = zpBuildName;
        this.zpCommunityId = zpCommunityId;
    }
}
