package com.css.springboottest.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Materials {
private PbrMetallicRoughness pbrMetallicRoughness;
private int[] emissiveFactor;
private String alphaMode;
private boolean doubleSided;
private String name;


}
