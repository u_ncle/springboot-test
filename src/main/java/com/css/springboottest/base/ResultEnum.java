package com.css.springboottest.base;

import lombok.AllArgsConstructor;
import lombok.Getter;

//创建枚举统一格式
//枚举就是针对一个个属性，创建一个个实例，类似于单例模式
@Getter
//所有参数的构造器
@AllArgsConstructor
public enum ResultEnum {
    SUCCESS(200, "成功"),
    ERROR(999, "失败");

    private Integer code;
    private String desc;

}
