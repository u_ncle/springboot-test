package com.css.springboottest.Thread;

import com.css.springboottest.util.GltfRead;
import com.css.springboottest.util.IOUtils;

import java.util.ArrayList;
import java.util.List;
//信号灯读写

public class ThreadPC {
    public static void main(String[] args) {
        SynContainer synContainer=new SynContainer();
        List<String>fileNames=IOUtils.getFiles("D:\\testClick");
            new ReadGltf(synContainer,fileNames).start();

            new WriteGltf(synContainer,fileNames).start();
        }

}
    class ReadGltf extends Thread{
        SynContainer synContainer;
        List<String>fileNames;
        public ReadGltf(SynContainer synContainer,List<String>fileNames){
            this.synContainer=synContainer;
            this.fileNames=fileNames;
        }

        @Override
        public void run() {
            for (int i = 0; i <fileNames.size(); i++) {
                synContainer.readFile(fileNames.get(i));
//                try {
//                    Thread.sleep(4000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

            }
        }
    }
    class WriteGltf extends Thread{
        SynContainer synContainer;
        List<String>FileNames;
        public WriteGltf(SynContainer synContainer,List<String>FileNames){
            this.synContainer=synContainer;
            this.FileNames=FileNames;

        }

        @Override
        public void run() {
            for (int i = 0; i <FileNames.size(); i++) {
                synContainer.writeFile();
            }
        }
    }

     class SynContainer extends Thread{

//         private RedisUtils redisUtils = BeanUtils.getBean(RedisUtils.class);
        List<String>container;
        String BH;
        String fileName;
        boolean flag=true;  //为true时读  为false时写
        //读取文件
        public synchronized void  readFile(String fileName){
            List<String>res=new ArrayList<>();
            //flag为false 停止读取，开始写入文件
            if(!flag){
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
                res= GltfRead.getWindowJson(fileName);
                System.out.println("信号灯读取："+fileName);
                this.notifyAll();
//                this.BH= IOUtils.getBuildingBH(fileName);
                this.container=res;
                this.fileName=fileName;
                this.flag=!this.flag;



        }
        //写入redis
        public synchronized void writeFile(){
            if(flag){
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
                System.out.println("信号灯写入"+fileName);
//                try {
//                    for (int i = 0; i <this.container.size(); i++) {
//                        redisUtils.set(this.BH+"win"+i,this.container.get(i));
////                        System.out.println("信号灯写入"+this.BH+"win"+i);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }finally {
                    this.notifyAll();
                    this.flag=!this.flag;
//                }

        }
    }




