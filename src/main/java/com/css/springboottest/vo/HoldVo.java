package com.css.springboottest.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HoldVo {
    private String zpRoomId;
    private String zpHouseholdId;
    private String zpHouseholdName;
    private String zpHouseholdIdentity;
    private String zpHouseholdPhone;
    private String zpHouseholdDangerType;
    private String zpHouseholdState;
    private String zpHouseholdPolitics;
    private Integer zpHouseholdSex;
    private Integer zpHouseholdAge;
    private String zpHouseholdAdress;
    //tips由String转为List<String>
    private String[] zpHouseholdTips;
    private String zpHouseholdCriminal;
    private String zpHouseholdOther;
    private String zpHouseholdMeasure;
    private String zpHouseholdHelp;


}
