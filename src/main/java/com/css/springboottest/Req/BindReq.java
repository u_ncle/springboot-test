package com.css.springboottest.Req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
@ApiModel("窗户绑定回传请求参数")
public class BindReq implements Serializable {
    @ApiModelProperty("房间id")
    private String roomId;
    @ApiModelProperty("窗户id")
    private List<String>windowId;


}
