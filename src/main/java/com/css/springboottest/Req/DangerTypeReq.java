package com.css.springboottest.Req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@Data
@ApiModel("getDangerWindows请求参数")
public class DangerTypeReq implements Serializable {
    @ApiModelProperty("危险类型")
    //治安危险类型
    private  String zp_household_danger_type;
    //建筑栋号
    @ApiModelProperty("建筑名称")
    private  String zp_build_name;
}
