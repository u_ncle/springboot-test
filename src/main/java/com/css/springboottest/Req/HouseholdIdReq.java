package com.css.springboottest.Req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@Data
@ApiModel("更新文本编辑器请求参数")
public class HouseholdIdReq implements Serializable {
    @ApiModelProperty("身份证号")
    //身份证号
    private String zp_household_id;
    @ApiModelProperty("更新文本(String) 犯罪经历")
    //更新文本(String) 犯罪经历
    private String criminal_info;
    @ApiModelProperty("更新文本(String) 其他说明")
    //更新文本(String) 其他说明
    private String other_info;
    @ApiModelProperty("更新文本(String) 管控措施")
   //  更新文本(String) 管控措施
    private String measure_info;
    @ApiModelProperty("更新文本(String) 帮扶政策")
    //  更新文本(String) 帮扶政策
    private String help_info;


}
