package com.css.springboottest.Req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("建筑信息更新回传参数")
public class BuildInfoReq  implements Serializable {
    @ApiModelProperty("建筑名称")
    private  String buildName;
    @ApiModelProperty("建筑信息")
    private List<String>buildInfos;
}
