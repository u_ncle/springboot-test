package com.css.springboottest.Req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("解除绑定参数")
public class UnbindReq {
    @ApiModelProperty("房间名")
    private String roomId;
}
