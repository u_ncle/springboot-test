package com.css.springboottest.util;

import com.sun.org.apache.regexp.internal.RE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class IOUtils {
    public static void main(String[] args) {
//        String path="D:\\A5 Split";
//        //调用方法
//        List<String>res=getFiles(path);
//        for (int i = 0; i < res.size(); i++) {
//            System.out.println(res.get(i));
//        }

        String fileName="D:\\environment\\apache-tomcat-9.0.53\\webapps\\ROOT\\A5Split\\jianzhu_A5_1_window.gltf";
        String BH=getBuildingBH(fileName);
        System.out.println(BH);

    }

//递归获取文件夹下的文件名称
    public static List<String> getFiles(String path) {
        List<String> fileNames=new ArrayList<>();
        File file = new File(path);
        // 如果这个路径是文件夹
        if (file.isDirectory()) {
            // 获取路径下的所有文件
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                // 如果还是文件夹 递归获取里面的文件 文件夹
                if (files[i].isDirectory()) {
                    System.out.println("目录：" + files[i].getPath());
                    getFiles(files[i].getPath());
                } else {
                    fileNames.add(files[i].getPath());
                }
            }
        } else {
            fileNames.add(file.getPath());
        }
        return fileNames;
    }

    //根据路径获取楼栋编号   公共部分 jianzhu window/building
    public static String getBuildingBH(String filePath){
        String buildingBH="";
        int start=filePath.lastIndexOf("\\");
        int end=filePath.lastIndexOf(".");
        filePath=filePath.substring(start+1,end);
        filePath=filePath.replace("jianzhu_","");
        if(filePath.indexOf("window")!=-1){
           filePath=filePath.replace("_window","");
           buildingBH=filePath.trim();

        }else if(filePath.indexOf("building")!=-1){
            filePath=filePath.replace("_building","");
            buildingBH=filePath.trim();
        }

        return buildingBH;
    }


}
