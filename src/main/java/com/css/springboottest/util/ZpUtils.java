package com.css.springboottest.util;

import com.css.springboottest.constant.ParmaConstant;

import java.util.*;

public class ZpUtils {
    public static void main(String[] args) {
    }
    public static List<String> getAllUnitList(List<String> rooms) {
        List<String>unitList=new ArrayList<>();
        Set unitSet=new HashSet();
        String[] buildList=ParmaConstant.buildNames;
        for (int i = 0; i < rooms.size(); i++) {
            String roomId=rooms.get(i);
            for (int j = 0; j < buildList.length; j++) {
//                房号匹配楼栋号
                if(roomId.indexOf(buildList[j])!=-1){

                    int Index=roomId.lastIndexOf(buildList[j]);
                    int length=buildList[j].length();
                    String unit=roomId.substring(Index+length+1,Index+length+2);
                    if(unitSet.add(buildList[j]+unit)){
                        unitList.add(buildList[j]+unit);
                    }

                }
            }
        }
        return unitList;
    }
    public static List<String> getUnitList(List<String> rooms) {
        List<String>unitList=new ArrayList<>();
        Set unitSet=new HashSet();
        String[] buildList=ParmaConstant.buildNames;
        for (int i = 0; i < rooms.size(); i++) {
            String roomId=rooms.get(i);
            for (int j = 0; j < buildList.length; j++) {
//                房号匹配楼栋号
                if(roomId.indexOf(buildList[j])!=-1){

                   int Index=roomId.lastIndexOf(buildList[j]);
                   int length=buildList[j].length();
                    String unit=roomId.substring(Index+length+1,Index+length+2);
                    if(unitSet.add(unit)){
                        unitList.add(unit);
                    }

                }
            }
        }
        return unitList;
    }
    public static int getUnitNum(List<String>unitList){
        return unitList.size();
    }
    public static List<String>getRoomNameList(List<String> rooms){
        List<String>roomNameList=new ArrayList<>();

        String[] buildList=ParmaConstant.buildNames;
        for (int i = 0; i < rooms.size(); i++) {
            String roomId=rooms.get(i);
            for (int j = 0; j < buildList.length; j++) {
                if(roomId.indexOf(buildList[j])!=-1){
                    int lastIndex=roomId.lastIndexOf("_");
                    roomNameList.add(roomId.substring(lastIndex+1));
                }
            }
        }
        return roomNameList;

    }
    public static int getRoomNum(List<String>roomNameList){
        return roomNameList.size();
    }
    //获取房间名称
    public static String getRoomName(String roomId){
        String roomName="";
        String[] buildList=ParmaConstant.buildNames;
        for (int j = 0; j < buildList.length; j++) {
            if(roomId.indexOf(buildList[j])!=-1){
                int lastIndex=roomId.lastIndexOf("_");
                roomName= roomId.substring(lastIndex+1);
                break;
            }
       }
        return roomName;
    }
    //获取楼栋名称
    public static String getBuildName(String roomId){
        String buildName="";
        String[] buildList=ParmaConstant.buildNames;
        for (int j = 0; j < buildList.length; j++) {
            if(roomId.indexOf(buildList[j])!=-1){
                buildName= buildList[j];
                break;
            }
        }
        return buildName;
    }
    //获取单元名称
    public static String getUnitName(String roomId){
        String unitName="";
        String[] buildList=ParmaConstant.buildNames;
        for (int j = 0; j < buildList.length; j++) {
//                房号匹配楼栋号
            if(roomId.indexOf(buildList[j])!=-1){

                int Index=roomId.lastIndexOf(buildList[j]);
                int length=buildList[j].length();
                String unit=roomId.substring(Index+length+1,Index+length+2);
                unitName=unit;
                break;
            }
        }
        return unitName;
    }
}

