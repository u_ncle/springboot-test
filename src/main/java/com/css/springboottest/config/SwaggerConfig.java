package com.css.springboottest.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

import static com.alibaba.fastjson.JSONPath.paths;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket(Environment environment){


        Profiles profiles=Profiles.of("dev");
        boolean flag= environment.acceptsProfiles(profiles);
//        获取项目环境 开发/生产
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .groupName("沙盘api0")
                .enable(flag)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.css.springboottest.controller"))
//                .paths(PathSelectors.ant("/household/**"))
                .paths(Predicates.or(PathSelectors.ant("/household/**"),PathSelectors.ant("/room/**"),PathSelectors.ant("/build/**")))
                .build()
                ;
    }
    private ApiInfo apiInfo=  new ApiInfo(
            "zp-api 文档",
            "后台接口文档",
            "1.0",
            "urn:tos",
            new Contact("css","",""),
            "Apache 2.0",
            "http://www.apache.org/licenses/LICENSE-2.0",
            new ArrayList());

}
