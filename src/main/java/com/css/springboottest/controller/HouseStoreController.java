package com.css.springboottest.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.springboottest.pojo.GltfModel;
import com.css.springboottest.service.HouseStoreService;
import com.css.springboottest.service.RedisWindowService;
import com.css.springboottest.service.impl.HouseStoreServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@RestController
@RequestMapping("/houseStore")
public class HouseStoreController {
    @Autowired
    private HouseStoreService houseStoreService;
    @GetMapping("/allHouse")
    public String houseList(){

        String resHouse= houseStoreService.getAllHouse();

        return resHouse;
    }



}

