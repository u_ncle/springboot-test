package com.css.springboottest.controller;


import com.css.springboottest.Req.DangerTypeReq;
import com.css.springboottest.Req.HouseholdIdReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.service.HouseholdService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */

@RestController
@RequestMapping("/household")
public class HouseholdController {
    @Autowired
    HouseholdService householdService;
//    @PostMapping("/getdanger")
//    public Result getDanger( @RequestBody DangerTypeReq req){
//        return  householdService.getAllDangerType(req);
//    }
    @ApiOperation("初始场景统计")
    @GetMapping("/getSum")
    public Result getSumStatics(){
        return householdService.getSum();
    }
    @ApiOperation("初始场景危险类型统计")
    @GetMapping("/getDangerType")
    public Result getDangerType(){
        return householdService.getDangerType();
    }
    @ApiOperation("某栋建筑统计")
    @GetMapping("/getOneSum/{buildName}")
    public Result getOneBuildSum(@ApiParam("建筑名称") @PathVariable String buildName){
        return householdService.getBuildSum(buildName);
    }
    @ApiOperation("某栋建筑危险类型统计")
    @GetMapping("/getOneDanger/{buildName}")
    public Result getOneDanger(@ApiParam("建筑名称") @PathVariable String buildName){
        return householdService.getBuildDangerType(buildName);
    }
    @ApiOperation("点击危险类型关联窗户")
    @PostMapping("/getDangerWindows")
    public Result getDangerWindows(@RequestBody DangerTypeReq req){
        return householdService.getDangerWindows(req);
    }
    @ApiOperation("点击窗户获取详细信息")
    @GetMapping("/{windowId}")
    public Result getMasterInfo( @ApiParam("窗户id") @PathVariable String windowId){
        return householdService.getMasterInfo(windowId);
    }
    @ApiOperation("更新住户信息")
    @PutMapping("")
    public Result updateHouseholdInfo(@RequestBody HouseholdIdReq householdIdReq){
        return householdService.updateHouseholdInfo(householdIdReq);
    }






}

