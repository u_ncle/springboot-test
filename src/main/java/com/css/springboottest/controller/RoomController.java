package com.css.springboottest.controller;


import com.css.springboottest.Req.BindReq;
import com.css.springboottest.Req.UnbindReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.service.RoomService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@RestController
@RequestMapping("/room")
public class RoomController {
    @Autowired
    RoomService roomService;
    @ApiOperation("绑定窗户的建筑树结构")
    @GetMapping("/getBuildTree/{buildName}")
    public Result getBuildTree(@ApiParam("建筑名称") @PathVariable String buildName){
        return roomService.getBuildTree(buildName);
    }
    @ApiOperation("绑定窗户回传函数")
   @PostMapping("/bindWindow")
    public Result bindWindow(@ApiParam("绑定参数")@RequestBody BindReq bindReq){
        return roomService.bindWindow(bindReq);
   }

   @ApiOperation("解除绑定")
    @PostMapping("/unBindWindow")
    public Result unBindWindow(@ApiParam("解绑参数")@RequestBody UnbindReq unbindReq){
        return roomService.unBindWindow(unbindReq);
   }
   @ApiOperation("返回已绑定窗户id")
    @GetMapping("/getBindWindow/{buildName}")
    public Result getBindWindow(@ApiParam("建筑名称") @PathVariable String buildName){
        return roomService.getBindWindow(buildName);
   }

}

