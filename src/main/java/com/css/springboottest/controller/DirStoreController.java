package com.css.springboottest.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Controller
@RequestMapping("/dirStore")
public class DirStoreController {

}

