package com.css.springboottest.controller;


import com.alibaba.fastjson.JSONObject;
import com.css.springboottest.pojo.GltfModel;
import com.css.springboottest.service.ClickWindowService;
import com.css.springboottest.service.RedisWindowService;
import com.css.springboottest.service.WindowStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@RestController
@RequestMapping("/windowStore")
public class WindowStoreController {
    @Autowired
    private WindowStoreService windowStoreService;
    @GetMapping("/allWindow")
    public List<GltfModel> windowList(){
        List<GltfModel>res=new ArrayList<>();
        List<String>resWindow=windowStoreService.getAllWindow();
        for (int i = 0; i < resWindow.size(); i++) {
            GltfModel gltfModel= JSONObject.parseObject(resWindow.get(i),GltfModel.class);
            res.add(gltfModel);
        }
        return res;
    }
    @GetMapping("/windowById/{id}")
    public String windowOnject(@PathVariable int id){
        return windowStoreService.findWindowById(id);
    }

//    @Autowired
//    private RedisWindowService redisWindowService;
//    @GetMapping("/redisWindow/{id}")
//    public String getRedisWindow(@PathVariable String id){
//        return redisWindowService.getRedisWindow(id);
//    }
//
//    @Autowired
//    private ClickWindowService clickWindowService;
//    @GetMapping("/windowUrls/{id}")
//    public Map getWindowUrls(@PathVariable String id){
//        return clickWindowService.getWindowUrls(id);
//    }


}

