package com.css.springboottest.controller;

import com.css.springboottest.service.ClickWindowService;
import com.css.springboottest.service.RedisWindowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/redisWindow")
public class RedisWindowController {
    @Autowired
    private RedisWindowService redisWindowService;
    @GetMapping("/window/{id}")
    public String getRedisWindow(@PathVariable String id){
        return redisWindowService.getRedisWindow(id);
    }

    @Autowired
    private ClickWindowService clickWindowService;
    @GetMapping("/windowUrls/{id}")
    public Map getWindowUrls(@PathVariable String id){
        return clickWindowService.getWindowUrls(id);
    }

}
