package com.css.springboottest.controller;


import com.css.springboottest.Req.BuildInfoReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.service.BuildService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@RestController
@RequestMapping("/build")
public class BuildController {
    @Autowired
    BuildService buildService;
    @ApiOperation("更新建筑信息")
    @PostMapping("")
    public Result updateBuildInfo(@ApiParam("建筑信息回传参数") @RequestBody BuildInfoReq buildInfoReq){
     return buildService.updateBuildInfo(buildInfoReq);
    }
    @ApiOperation("获取建筑信息")
    @GetMapping("/{buildName}")
    public Result getBuildInfo(@ApiParam("建筑名称") @PathVariable String buildName ){
        return buildService.getBuildInfo(buildName);
    }

}

