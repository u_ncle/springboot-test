package com.css.springboottest.service.impl;

import com.css.springboottest.Req.BindReq;
import com.css.springboottest.Req.UnbindReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.pojo.Room;
import com.css.springboottest.mapper.RoomMapper;
import com.css.springboottest.pojo.TreeInfo;
import com.css.springboottest.pojo.TreeNode;
import com.css.springboottest.pojo.TreeParent;
import com.css.springboottest.service.RoomService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.css.springboottest.util.ZpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Service
public class RoomServiceImpl extends ServiceImpl<RoomMapper, Room> implements RoomService {
    private List<TreeParent> parentArrayList = new ArrayList<>();
    private List<TreeInfo> infoArrayList = new ArrayList<>();

    @Autowired
    RoomMapper roomMapper;

    public List<String> getRoomList(String buildName) {
       return roomMapper.getRoomList(buildName);
    }

    //初始化窗户绑定节点数据
    public Result getBuildTree(String buildName){
        //设置rootid为0
        TreeNode build= null;
        try {
            build = new TreeNode("",buildName,-1);
            //获取某栋的单元列表
            List<String>unitList= ZpUtils.getUnitList(getRoomList(buildName));
            String buildUid=UUID.randomUUID().toString().replaceAll("-","");


            if(unitList.size()>0) {

                for (int i = 0; i < unitList.size(); i++) {
                    String unitUid = UUID.randomUUID().toString().replaceAll("-", "");
                    TreeParent unitParent = new TreeParent(unitUid, buildUid, unitList.get(i) + "单元", -1);
                    parentArrayList.add(unitParent);
                    //获取某栋不同单元下的房号列表   设置isbind属性
                    List<String> roomsInUnit = roomMapper.getRoomsWithUnit(buildName, unitList.get(i));
                    if (roomsInUnit.size() > 0) {
                    for (int j = 0; j < roomsInUnit.size(); j++) {
                        String roomUid = UUID.randomUUID().toString().replaceAll("-", "");
                        TreeParent uroomParent = new TreeParent(roomUid, unitUid, roomsInUnit.get(j), 0);
                        parentArrayList.add(uroomParent);
                        //获取与房号关联的窗户    设置isbind属性
                        List<String> windowJoinRoom = roomMapper.getWindowJoinRoom(roomsInUnit.get(j));
                        if (windowJoinRoom.size() > 0&&(windowJoinRoom.get(0)!=null&&windowJoinRoom.get(0).length()!=0)) {
                            uroomParent.setIsBind(1);
                        for (int k = 0; k < windowJoinRoom.size(); k++) {
                            String windowUid = UUID.randomUUID().toString().replaceAll("-", "");
                            TreeInfo treeInfo = new TreeInfo(windowUid, roomUid, windowJoinRoom.get(k), 1);
                            infoArrayList.add(treeInfo);
                        }
                    }
                    }
                }
                    //获取与房号关联的窗户
                }
            }
            initTreeRoot(build,buildUid);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("获取数据失败");
        }

        return Result.ok(build);

    }
    //

   //  初始化窗户绑定叶子数据
    public void initLeafData(){
    }
//    构造树
    public void buildTree(){
    }
    //点击某一户列表高亮窗户（前端）

    //点击绑定设isbind字段为1
    public void bindRoom(){
    }
    public void initTreeRoot(TreeNode parent, String parentId){
        for (int i = 0; i < parentArrayList.size(); i++) {
            TreeParent treeParent=parentArrayList.get(i);
            if(parentId.equals(treeParent.getParentId())){
                TreeNode treeNode=new TreeNode(parent.getName(),treeParent.getName(),treeParent.getIsBind());
                initTreeRoot(treeNode,treeParent.getId());
                parent.addChildNode(treeNode);
            }
        }
        initTreeChild(parent,parentId);

    }
    public void initTreeChild(TreeNode parent,String groupId){
        for (int i = 0; i < infoArrayList.size(); i++) {
            TreeInfo treeInfo=infoArrayList.get(i);
            if(groupId.equals(treeInfo.getGroupId())){
                TreeNode treeNode=new TreeNode(parent.getName(),treeInfo.getName(),treeInfo.getIsBind());
                parent.addChildNode(treeNode);
            }

        }
    }

    @Override
    public Result bindWindow(BindReq bindReq) {
        String roomId=bindReq.getRoomId();
        List<String>windowsId=bindReq.getWindowId();
        try {
            for (int i = 0; i < windowsId.size(); i++) {
                roomMapper.updateWindow(roomId,windowsId.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("绑定失败");
        }
        return Result.ok("绑定成功");
    }

    @Override
    public Result unBindWindow(UnbindReq unbindReq) {
        try {
            roomMapper.unBindWindow(unbindReq.getRoomId());
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("解绑出错");
        }
        return Result.ok("解绑成功");
    }

    @Override
    public Result getBindWindow(String buildName) {
        List<String>bindWindows= null;
        try {
            bindWindows = roomMapper.getBindWindow(buildName);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询失败");
        }

        return Result.ok(bindWindows);
    }
}
