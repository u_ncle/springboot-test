package com.css.springboottest.service.impl;

import com.css.springboottest.pojo.WindowStore;
import com.css.springboottest.mapper.WindowStoreMapper;
import com.css.springboottest.service.WindowStoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Service
public class WindowStoreServiceImpl extends ServiceImpl<WindowStoreMapper, WindowStore> implements WindowStoreService {
@Autowired
    WindowStoreMapper windowStoreMapper;

    @Override
    public List<String> getAllWindow() {
        return windowStoreMapper.getAllWindow();
    }

    @Override
    public String findWindowById(int id) {
        return windowStoreMapper.findWindowById(id);
    }
}
