package com.css.springboottest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.css.springboottest.Req.DangerTypeReq;
import com.css.springboottest.Req.HouseholdIdReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.constant.ParmaConstant;
import com.css.springboottest.pojo.DangerTypeStatics;
import com.css.springboottest.pojo.Household;
import com.css.springboottest.mapper.HouseholdMapper;
import com.css.springboottest.pojo.SumStatistics;
import com.css.springboottest.service.HouseholdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.css.springboottest.util.ZpUtils;
import com.css.springboottest.vo.HoldVo;
import com.github.jeffreyning.mybatisplus.service.MppServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Service
public class HouseholdServiceImpl extends MppServiceImpl<HouseholdMapper, Household> implements HouseholdService {
    @Autowired HouseholdMapper householdMapper;
//    @Override
//    public Result getAllDangerType(DangerTypeReq req) {
//        QueryWrapper query=new QueryWrapper();
//        if(req!=null){
//            if (StringUtils.isNotBlank(req.getZp_household_danger_type())){
//                query.like("zp_household_danger_type",req.getZp_household_danger_type());
//            }
//        }
//        List<Household>householdList=householdMapper.selectList(query);
//        return Result.ok(householdList);
//    }



    public int getparkNum() {
        return householdMapper.staticsParkNum();
    }


    public int getBuildNum() {
        return  householdMapper.staticeBuildNum();
    }


    public int getUnitNum() {
        List<String>unitList= ZpUtils.getAllUnitList(householdMapper.getRooms());
        return unitList.size();
    }


    public int getRoomNum() {
        return householdMapper.staticsRoomNum();
    }


    public int getPopulation() {
        return householdMapper.staticsPopulation();
    }

    public Result getSum(){
        SumStatistics sumStatistics= null;
        try {
            int parkNum=getparkNum();
            int buildNum=getBuildNum();
            int unitNum=getUnitNum();
            int roomNum=getRoomNum();
            int population=getPopulation();
            sumStatistics = new SumStatistics(parkNum,buildNum,unitNum,roomNum,population);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("发生错误查询失败");
        }
            return Result.ok(sumStatistics);


    }

    @Override
    public Result getDangerType() {
        List<DangerTypeStatics> dangerList= null;
        try {
            dangerList = householdMapper.staticsDangerNum();
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("发生错误查询失败");
        }
            return Result.ok(dangerList);


    }
    //单栋----------------------------------------------------
    public int getOneBuildNum(String buildName){
        return householdMapper.getOneBuildNum(buildName);
    }
    public int getOneBuildUnit(String buildName){
        List<String>oneBuildUnits= householdMapper.getOneBuildRooms(buildName);
        List<String>unitList=ZpUtils.getUnitList(oneBuildUnits);
        return unitList.size();
    }
    public int getOneBuildRoomNum(String buildName){
        return householdMapper.getOneBuildRoomNum(buildName);
    }
    public int getOneBuildPopulation(String buildName){
        return householdMapper.getOneBuildPopulation(buildName);
    }

    @Override
    public Result getBuildSum(String buildName) {
        SumStatistics sumStatistics= null;
        try {
            int oneBuildNum=getOneBuildNum(buildName);
            int oneBuildUnit=getOneBuildUnit(buildName);
            int oneBuildRoomNum=getOneBuildRoomNum(buildName);
            int oneBuildPopulation=getOneBuildPopulation(buildName);
            sumStatistics = new SumStatistics(oneBuildNum,oneBuildUnit,oneBuildRoomNum,oneBuildPopulation);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("发生错误查询失败");
        }
            return Result.ok(sumStatistics);

    }
    @Override
    public Result getBuildDangerType(String buildName) {
        List<DangerTypeStatics> dangerTypeStatics= null;
        try {
            dangerTypeStatics = householdMapper.getOneBuildDangerNum(buildName);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("发生错误查询失败");
        }
            return Result.ok(dangerTypeStatics);


    }

    @Override
    public Result getDangerWindows(DangerTypeReq dangerTypeReq) {
        List<String>dangerWindows= null;
        try {
            dangerWindows = householdMapper.getDangerWindows(dangerTypeReq);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("发生错误查询失败");
        }
            return Result.ok(dangerWindows);

    }

    @Override
    public Result getMasterInfo(String windowId) {
      //数据库实体类
        List<Household>masterList= null;
        Household household=null;
        //视图层实体类
        List<HoldVo>masterVoList=new ArrayList<>();
        try {
            masterList = householdMapper.getHouseholdList(windowId);
                for (int i = 0; i < masterList.size(); i++) {
                if(masterList.get(0)!=null) {
                    household = masterList.get(i);
                    String[] tipsList = {};
                    //分解tips
                    if (household.getZpHouseholdTips() != null && household.getZpHouseholdTips().length() != 0) {
                        String tips = (household.getZpHouseholdTips()).trim();
                        if (household.getZpHouseholdTips().indexOf(",") != -1) {
                            tipsList = tips.split(",");
                        } else {
                            tipsList[0] = tips;
                        }
                    }

                    HoldVo holdVo = new HoldVo(
                            household.getZpRoomId(),
                            household.getZpHouseholdId(),
                            household.getZpHouseholdName(),
                            household.getZpHouseholdIdentity(),
                            household.getZpHouseholdPhone(),
                            household.getZpHouseholdDangerType(),
                            household.getZpHouseholdState(),
                            household.getZpHouseholdPolitics(),
                            household.getZpHouseholdSex(),
                            household.getZpHouseholdAge(),
                            household.getZpHouseholdAdress(),
                            tipsList,
                            household.getZpHouseholdCriminal(),
                            household.getZpHouseholdOther(),
                            household.getZpHouseholdMeasure(),
                            household.getZpHouseholdHelp()
                    );
                    masterVoList.add(holdVo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("发生错误查询失败");
        }
            return Result.ok(masterVoList);


    }

    @Override
    public Result updateHouseholdInfo(HouseholdIdReq householdIdReq) {
        try {
            householdMapper.updateHouseholdInfo(householdIdReq);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("发生错误更新失败");
        }
            return Result.ok("更新成功");


    }
}
