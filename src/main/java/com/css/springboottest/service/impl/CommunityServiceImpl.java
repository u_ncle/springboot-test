package com.css.springboottest.service.impl;

import com.css.springboottest.pojo.Community;
import com.css.springboottest.mapper.CommunityMapper;
import com.css.springboottest.service.CommunityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Service
public class CommunityServiceImpl extends ServiceImpl<CommunityMapper, Community> implements CommunityService {

}
