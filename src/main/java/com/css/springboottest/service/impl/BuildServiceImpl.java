package com.css.springboottest.service.impl;

import com.css.springboottest.Req.BuildInfoReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.pojo.Build;
import com.css.springboottest.mapper.BuildMapper;
import com.css.springboottest.service.BuildService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Service
public class BuildServiceImpl extends ServiceImpl<BuildMapper, Build> implements BuildService {
    @Autowired BuildMapper buildMapper;
    @Override
    public Result updateBuildInfo(BuildInfoReq buildInfoReq) {
        try {
            String buildName=buildInfoReq.getBuildName();
            List<String>buildInfos=buildInfoReq.getBuildInfos();
            StringBuffer sb=new StringBuffer();
            if(buildInfos.size()>0){
                for (int i = 0; i < buildInfos.size()-1; i++) {
                    sb.append(buildInfos.get(i).trim()).append(",");
                }
                sb.append(buildInfos.get(buildInfos.size()-1));
            }

            buildMapper.UpdateBuildInfo(buildName,sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return  Result.error("更新失败");
        }
        return Result.ok("更新建筑信息成功");
    }

    @Override
    public Result getBuildInfo(String buildName) {
         List<String>infoList= new ArrayList<>();
        try {
            String buildInfos=buildMapper.getBuildInfo(buildName);
            if(buildInfos!=null&&buildInfos.length()!=0){
                if(buildInfos.indexOf(",")!=-1){
                  String[] infos = buildInfos.split(",");
                  infoList= Arrays.asList(infos);
                }else {
                    infoList.add(buildInfos);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询失败");
        }
        return Result.ok(infoList);
    }
}
