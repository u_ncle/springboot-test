package com.css.springboottest.service.impl;

import com.css.springboottest.pojo.DirStore;
import com.css.springboottest.mapper.DirStoreMapper;
import com.css.springboottest.service.DirStoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Service
public class DirStoreServiceImpl extends ServiceImpl<DirStoreMapper, DirStore> implements DirStoreService {

}
