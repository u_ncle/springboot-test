package com.css.springboottest.service.impl;

import com.css.springboottest.service.RedisWindowService;
import com.css.springboottest.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedisWindowServiceImpl implements RedisWindowService {
    @Autowired
    RedisUtils redisUtils;

    @Override
    public String getRedisWindow(String i) {
      return (String) redisUtils.get(i);
    }
}
