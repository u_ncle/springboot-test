package com.css.springboottest.service.impl;

import com.css.springboottest.pojo.Window;
import com.css.springboottest.mapper.WindowMapper;
import com.css.springboottest.service.WindowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Service
public class WindowServiceImpl extends ServiceImpl<WindowMapper, Window> implements WindowService {

}
