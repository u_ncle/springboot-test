package com.css.springboottest.service.impl;

import com.css.springboottest.pojo.GltfModel;
import com.css.springboottest.pojo.HouseStore;
import com.css.springboottest.mapper.HouseStoreMapper;
import com.css.springboottest.service.HouseStoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Service
public class HouseStoreServiceImpl extends ServiceImpl<HouseStoreMapper, HouseStore> implements HouseStoreService {
    @Autowired
    HouseStoreMapper houseStoreMapper;
    @Override
    public String getAllHouse() {
      return houseStoreMapper.getAllHouse();
    }

}
