package com.css.springboottest.service.impl;

import com.css.springboottest.service.ClickWindowService;
import com.css.springboottest.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
@Service
public class ClickWindowServiceImpl implements ClickWindowService {
    @Autowired
    RedisUtils redisUtils;
    @Override
    public Map<String,Set> getWindowUrls(String buildBH) {
        Map<String,Set>windowUrls=new HashMap<>();
        Set<Object> windowBH=redisUtils.sGet(buildBH);
        windowUrls.put(buildBH,windowBH);
        return windowUrls;

    }
}
