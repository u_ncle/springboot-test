package com.css.springboottest.service;

import com.css.springboottest.pojo.WindowStore;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
public interface WindowStoreService extends IService<WindowStore> {

List<String>getAllWindow();

 String findWindowById( int id);
}
