package com.css.springboottest.service;

import com.css.springboottest.Req.BuildInfoReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.pojo.Build;
import com.baomidou.mybatisplus.extension.service.IService;
import com.css.springboottest.pojo.TreeInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
public interface BuildService extends IService<Build> {
    Result updateBuildInfo(BuildInfoReq buildInfoReq);
    Result getBuildInfo(String buildName);
}
