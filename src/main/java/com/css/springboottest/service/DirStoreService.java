package com.css.springboottest.service;

import com.css.springboottest.pojo.DirStore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
public interface DirStoreService extends IService<DirStore> {

}
