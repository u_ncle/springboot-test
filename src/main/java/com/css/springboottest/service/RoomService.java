package com.css.springboottest.service;

import com.css.springboottest.Req.BindReq;
import com.css.springboottest.Req.UnbindReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.pojo.Room;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
public interface RoomService extends IService<Room> {
    Result getBuildTree(String buildName);
    Result bindWindow(BindReq bindReq);
    Result unBindWindow(UnbindReq unbindReq);
    Result getBindWindow(String buildName);

}
