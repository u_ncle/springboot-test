package com.css.springboottest.service;

import com.css.springboottest.pojo.GltfModel;
import com.css.springboottest.pojo.HouseStore;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
public interface HouseStoreService extends IService<HouseStore> {
    String getAllHouse();

}
