package com.css.springboottest.service;

import com.css.springboottest.pojo.Community;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
public interface CommunityService extends IService<Community> {

}
