package com.css.springboottest.service;

import com.css.springboottest.Req.DangerTypeReq;
import com.css.springboottest.Req.HouseholdIdReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.pojo.Household;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.jeffreyning.mybatisplus.service.IMppService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
public interface HouseholdService extends IMppService<Household> {
    //初始场景
//    Result getAllDangerType(DangerTypeReq dangerTypeReq);
    Result getSum();
    Result getDangerType();
    //单栋
    Result getBuildSum(String buildName);
    Result getBuildDangerType(String buildName);
    //单栋危险窗户
    Result getDangerWindows(DangerTypeReq dangerTypeReq);
    //点击窗户查看户主信息
    Result getMasterInfo(String windowId);
    //更新住户信息
    Result updateHouseholdInfo(HouseholdIdReq householdIdReq);

}
