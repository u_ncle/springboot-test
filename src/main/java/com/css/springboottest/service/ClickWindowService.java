package com.css.springboottest.service;

import java.util.Map;
import java.util.Set;

public interface ClickWindowService {
Map<String,Set> getWindowUrls(String buildBH);
}
