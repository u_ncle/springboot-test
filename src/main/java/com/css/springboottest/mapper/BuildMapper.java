package com.css.springboottest.mapper;

import com.css.springboottest.pojo.Build;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Mapper
public interface BuildMapper extends BaseMapper<Build> {
    void UpdateBuildInfo(@Param("buildName")String buildName,@Param("buildIndo")String buildInfo);
    String getBuildInfo(@Param("buildName")String buildName);

}
