package com.css.springboottest.mapper;

import com.css.springboottest.pojo.DirStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Mapper
public interface DirStoreMapper extends BaseMapper<DirStore> {

//   选出A5-10的dir_id
@Select("select dir_id from zp_dir_store where dir_name like '%10%'")
String selectDirid();
}
