package com.css.springboottest.mapper;

import com.css.springboottest.Req.DangerTypeReq;
import com.css.springboottest.Req.HouseholdIdReq;
import com.css.springboottest.base.Result;
import com.css.springboottest.pojo.DangerTypeStatics;
import com.css.springboottest.pojo.Household;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.jeffreyning.mybatisplus.base.MppBaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Mapper
public interface HouseholdMapper extends MppBaseMapper<Household> {
    //初始场景
    List<DangerTypeStatics> staticsDangerNum();
    int staticsParkNum();
    int staticeBuildNum();
    int staticsRoomNum();
    int staticsPopulation();
    List<String> getRooms();
    //独栋场景
    int getOneBuildNum(@Param("buildName") String buildName);
    List<String>  getOneBuildRooms(@Param("buildName") String buildName);
    int getOneBuildRoomNum(@Param("buildName") String buildName);
    int getOneBuildPopulation(@Param("buildName") String buildName);
    List<DangerTypeStatics>getOneBuildDangerNum(@Param("buildName") String buildName);
    //独栋危险窗户
    List<String> getDangerWindows(@Param("dangerType")DangerTypeReq dangerType);
    //点击窗户查看户主信息
    List<Household>getHouseholdList(@Param("windowId")String windowId);
    //更新住户信息
    void updateHouseholdInfo(@Param("householdIdReq") HouseholdIdReq householdIdReq);;







}
