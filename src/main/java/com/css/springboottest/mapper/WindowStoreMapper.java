package com.css.springboottest.mapper;

import com.css.springboottest.pojo.WindowStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Mapper
public interface WindowStoreMapper extends BaseMapper<WindowStore> {
//    @Select("select json from zp_window_store ")
    List<String>getAllWindow();

    String findWindowById(@Param("id") int id);
}
