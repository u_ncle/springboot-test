package com.css.springboottest.mapper;

import com.css.springboottest.pojo.Window;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Mapper
public interface WindowMapper extends BaseMapper<Window> {

}
