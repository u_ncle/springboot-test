package com.css.springboottest.mapper;

import com.css.springboottest.Req.BindReq;
import com.css.springboottest.pojo.Room;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Mapper
public interface RoomMapper extends BaseMapper<Room> {
    List<String>getRoomList(@Param("buildName") String buildName);
    List<String>getRoomsWithUnit(@Param("buildName") String buildName,@Param("unitName")String unitName);
    List<String>getWindowJoinRoom(@Param("roomId")String roomId);
    void updateWindow(@Param("roomId") String roomId,@Param("windowId") String windowId);
    void unBindWindow(@Param("rooId")String roomId);
    List<String>getBindWindow(@Param("buildName")String buildName);

}
