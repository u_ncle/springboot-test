package com.css.springboottest.mapper;

import com.css.springboottest.pojo.Community;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author css
 * @since 2021-11-10
 */
@Mapper
public interface CommunityMapper extends BaseMapper<Community> {
    @Select("select zp_community_id from zp_community")
    String getCommunityId();

}
