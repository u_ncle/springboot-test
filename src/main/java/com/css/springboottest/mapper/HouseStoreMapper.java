package com.css.springboottest.mapper;

import com.css.springboottest.pojo.GltfModel;
import com.css.springboottest.pojo.HouseStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author css
 * @since 2021-10-09
 */
@Mapper
public interface HouseStoreMapper extends BaseMapper<HouseStore> {
//    @Select("select json from zp_house_store ")
  String getAllHouse();


}
